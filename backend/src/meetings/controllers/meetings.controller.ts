import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  Req,
  Res,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { ScheduledMeetingDTO } from 'src/db/dto/schedulemeeting.dto';
import { UserRole } from 'src/db/entities/user.entity';
import { MeetingsService } from '../services/meetings.service';

@Controller('meeting')
export class MeetingsController {
  constructor(private meetingService: MeetingsService) {}

  @Roles(UserRole.ALPHA, UserRole.ADMIN, UserRole.USER)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post()
  async createInstantMeeting(@Req() req, @Res() res) {
    const result = await this.meetingService.createInstantMeeting(req.user.id);
    res.status(result.code).json(result.content);
  }

  @UseGuards(JwtAuthGuard)
  @Post('scheduled')
  async createScheduledMeeting(@Body() body, @Res() res, @Req() req) {
    const meeting = new ScheduledMeetingDTO();
    meeting.title = body.title;
    meeting.attendeeIds = body.attendeeIds;

    try {
      const start_date = new Date(body.start_date);
      const end_date = new Date(body.end_date);

      meeting.start_date = start_date.toISOString();
      meeting.end_date = end_date.toISOString();

      const current_date = new Date(Date.now());

      if (current_date.getTime() > end_date.getTime()) {
        throw new BadRequestException(
          'Cannot create a scheduled meeting with end_date that has already passed',
        );
      }

      if (start_date.getTime() > end_date.getTime()) {
        throw new BadRequestException(
          'Cannot create a scheduled meeting with end_date time that is before the start_date time',
        );
      }

      await validate(meeting).then((errors) => {
        if (errors.length > 0) {
          throw new BadRequestException(
            errors[0].constraints[Object.keys(errors[0].constraints)[0]],
          );
        }
      });
    } catch (error) {
      throw new BadRequestException(error.message);
    }

    const result = await this.meetingService.createScheduledMeeting(
      req.user.id,
      meeting,
    );
    res.status(result.code).json(result.content);
  }

  @UseGuards(JwtAuthGuard)
  @Get('upcoming')
  async getUpcomingMeetings(@Res() res, @Req() req) {
    const result = await this.meetingService.getUpComingMeetings(req.user.id);
    res.status(result.code).json(result.content);
  }

  @UseGuards(JwtAuthGuard)
  @Get('ongoing')
  async getOngoingMeetings(@Res() res, @Req() req) {
    const result = await this.meetingService.getOnGoingMeetings(req.user.id);
    res.status(result.code).json(result.content);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/:id')
  async getMeetingById(@Res() res, @Param('id') id) {
    const result = await this.meetingService.getMeetingById(id);
    res.status(result.code).json(result.content);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async deleteMeetingById(@Res() res, @Param('id') id, @Req() req) {
    const { content: meeting } = await this.meetingService.getMeetingById(id);

    if (meeting.owner.id != req.user.id) throw new UnauthorizedException();

    const result = await this.meetingService.deleteMeetingById(id);

    res.status(result.code).json(result.content);
  }
}

@Controller('admin/meeting')
export class AdminMeetingsController {
  constructor(private meetingService: MeetingsService) {}

  @Roles(UserRole.ALPHA, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('all')
  async getAllMeeting(@Query() query, @Res() res) {
    let limit = 10;
    let page = 1;

    try {
      if (query.limit) limit = Number(query.limit);
      if (query.page) page = Number(query.page);
    } catch (error) {
      throw new BadRequestException('Page and Limit must be a digit');
    }

    if (limit <= 0) limit = 10;
    if (page <= 0) page = 1;

    // Offset the page for the DB query
    page -= 1;

    const result = await this.meetingService.getAllMeetings(limit, page);
    res.status(result.code).json(result.content);
  }

  @Roles(UserRole.ALPHA, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete('/:id')
  async deleteMeetingById(@Res() res, @Param('id') id) {
    const result = await this.meetingService.deleteMeetingById(id);

    res.status(result.code).json(result.content);
  }
}
