import { Module } from '@nestjs/common';
import { MeetingsService } from './services/meetings.service';
import {
  MeetingsController,
  AdminMeetingsController,
} from './controllers/meetings.controller';
import { User } from 'src/db/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Meeting } from 'src/db/entities/meeting.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Meeting])],
  providers: [MeetingsService],
  exports: [MeetingsService],
  controllers: [MeetingsController, AdminMeetingsController],
})
export class MeetingsModule {}
