import { ScheduledMeetingDTO } from 'src/db/dto/schedulemeeting.dto';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { User } from 'src/db/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Meeting } from 'src/db/entities/meeting.entity';

@Injectable()
export class MeetingsService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Meeting) private meetingsRepository: Repository<Meeting>,
  ) {}

  /**
   * Instant Meeting : Create an immediate meeting without the need of providing start and end time. Instant meeting will NOT be shown in any user's homepage. Instead it will be only accessible when the owner share the direct link to the meeting.
   */
  async createInstantMeeting(userId): Promise<Record<string, any>> {
    const owner = await this.usersRepository.findOne(userId);

    if (!owner)
      return {
        code: 401,
        content: { message: 'User attempt operation is not found!' },
      };

    const newMeeting = this.meetingsRepository.create({
      owner: owner,
    });

    await newMeeting.save();

    return { code: 200, content: newMeeting };
  }

  /**
   * Schedule Meeting : Create a meeting for later by providing start time, end time and ATTENDEES. The owner and the attendees who were invited will be able to see this meeting in their UPCOMING MEETING or CURRENTLY ON GOING page.
   */
  async createScheduledMeeting(
    userId,
    meetingDTO: ScheduledMeetingDTO,
  ): Promise<Record<string, any>> {
    const owner = await this.usersRepository.findOne(userId);

    if (!owner)
      return {
        code: 401,
        content: { message: 'User attempted operation is not found!' },
      };

    const attendees = await this.usersRepository.findByIds(
      meetingDTO.attendeeIds,
    );

    const newMeeting = this.meetingsRepository.create({
      title: meetingDTO.title,
      start_date: meetingDTO.start_date,
      end_date: meetingDTO.end_date,
      owner: owner,
      attendees: [owner, ...attendees],
    });

    await newMeeting.save();

    return { code: 200, content: newMeeting };
  }

  /**
   * Upcoming Meetings: Meeting that the users are invited and is yet to start
   */
  async getUpComingMeetings(userId): Promise<Record<string, any>> {
    const user = await this.usersRepository.findOne(userId, {
      relations: ['invitedMeetings', 'invitedMeetings.owner'],
    });

    if (!user)
      return {
        code: 401,
        content: { message: 'User attempted operation is not found!' },
      };

    const current_date = new Date();
    const invited_meetings = user.invitedMeetings;
    const upcoming_meetings = invited_meetings.filter((meeting) => {
      return new Date(meeting.start_date).getTime() > current_date.getTime();
    });

    return { code: 200, content: upcoming_meetings };
  }

  /**
   * Upcoming Meetings: Meeting that the users are invited and is in progressed
   */
  async getOnGoingMeetings(userId): Promise<Record<string, any>> {
    const user = await this.usersRepository.findOne(userId, {
      relations: ['invitedMeetings', 'invitedMeetings.owner'],
    });

    if (!user)
      return {
        code: 401,
        content: { message: 'User attempted operation is not found!' },
      };

    const current_date = new Date();
    const invited_meetings = user.invitedMeetings;
    const upcoming_meetings = invited_meetings.filter((meeting) => {
      return (
        new Date(meeting.start_date).getTime() < current_date.getTime() &&
        new Date(meeting.end_date).getTime() > current_date.getTime()
      );
    });

    return { code: 200, content: upcoming_meetings };
  }

  /**
   * Get the details of the meeting from the given id
   */
  async getMeetingById(id): Promise<Record<string, any>> {
    const meeting = await this.meetingsRepository.findOne(id, {
      relations: ['owner'],
    });

    if (!meeting)
      return {
        code: 404,
        content: { message: 'Meeting not found' },
      };

    return { code: 200, content: meeting };
  }

  /**
   * delete meeting from the given id
   */
  async deleteMeetingById(id): Promise<Record<string, any>> {
    const meeting = await this.meetingsRepository.findOne(id);

    if (!meeting)
      return {
        code: 200,
        content: {
          message: 'Meeting to be deleted is not found',
        },
      };

    await this.meetingsRepository.remove(meeting).catch((error) => {
      console.log(error);
      return {
        code: 500,
        content: {
          message: 'Something went wrong. Meeting could not be delete.',
        },
      };
    });

    return {
      code: 200,
      content: {
        message: 'Meeting was deleted as successfuly',
      },
    };
  }

  /**
   * Get all the available meetings of all type of status
   */
  async getAllMeetings(limit, page): Promise<Record<string, any>> {
    const meetings = await this.meetingsRepository.findAndCount({
      relations: ['owner'],
      skip: page * limit,
      take: limit,
      order: { createdAt: 'DESC' },
    });

    if (!meetings)
      return {
        code: 404,
        content: {
          message:
            'There was some problem during database query for all meetings',
        },
      };

    const total = meetings[1];

    if (limit > total) limit = total;

    page += 1;

    const lastPage = Math.ceil(total / limit);
    const hasNextPage = page < lastPage;

    return {
      code: 200,
      content: {
        meetings: meetings[0],
        total: total,
        page: page,
        lastPage: lastPage,
        hasNextPage: hasNextPage,
      },
    };
  }
}
