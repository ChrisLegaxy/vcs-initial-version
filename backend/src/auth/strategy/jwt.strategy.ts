import { UsersService } from '../../users/services/users.service';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: any) {
    const { content: user } = await this.usersService.findOne(payload.sub);
    if (!user)
      throw new UnauthorizedException('User attempted operation is not found');

    return {
      id: payload.sub,
      username: payload.username,
      role: user.role,
      displayName: user.displayName,
      phone: user.phone,
      email: user.email,
    };
  }
}
