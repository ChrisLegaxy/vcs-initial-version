import { Module } from '@nestjs/common';
import { UsersService } from './services/users.service';
import {
  AdminUsersController,
  UsersController,
} from './controllers/users.controller';
import { User } from 'src/db/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [UsersService],
  exports: [UsersService],
  controllers: [UsersController, AdminUsersController],
})
export class UsersModule {}
