import { RolesGuard } from '../../auth/guards/roles.guard';
import { UsersService } from 'src/users/services/users.service';
import {
  Body,
  Controller,
  Get,
  Res,
  Req,
  UseGuards,
  Post,
  BadRequestException,
  Put,
  UnauthorizedException,
  Delete,
  Param,
} from '@nestjs/common';
import { UserDTO } from 'src/db/dto/user.dto';
import { validate } from 'class-validator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { UserRole } from 'src/db/entities/user.entity';
import { UserInfoDTO } from 'src/db/dto/userinfo.dto';
import { UserNewPasswordDTO } from 'src/db/dto/usernewpassword.dto';
import * as bcrypt from 'bcrypt';

/// For User
@Controller('user')
export class UsersController {
  constructor(private userService: UsersService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  getProfile(@Req() req) {
    return req.user;
  }

  @UseGuards(JwtAuthGuard)
  @Get('all')
  async getAllUsers(@Res() res) {
    const result = await this.userService.getAllNonAdminUser();
    res.status(result.code).json(result.content);
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  async updateUserInfo(@Body() body, @Req() req, @Res() res) {
    const userInfo = new UserInfoDTO();

    userInfo.displayName = body.displayName;
    userInfo.email = body.email;
    userInfo.phone = body.phone;

    await validate(userInfo).then((errors) => {
      if (errors.length > 0) {
        throw new BadRequestException(
          errors[0].constraints[Object.keys(errors[0].constraints)[0]],
        );
      }
    });

    const result = await this.userService.updateUserInfo(req.user.id, userInfo);
    res.status(result.code).json(result.content);
  }

  @UseGuards(JwtAuthGuard)
  @Put('changepassword')
  async changePassword(@Body() body, @Req() req, @Res() res) {
    const userNewPassDTO = new UserNewPasswordDTO();
    userNewPassDTO.password = body.password;
    userNewPassDTO.newPassword = body.newPassword;

    await validate(userNewPassDTO).then((errors) => {
      if (errors.length > 0) {
        throw new BadRequestException(
          errors[0].constraints[Object.keys(errors[0].constraints)[0]],
        );
      }
    });

    if (userNewPassDTO.password == userNewPassDTO.newPassword)
      throw new BadRequestException(
        'You cannot set your new password to an old password!',
      );

    const { content: user } = await this.userService.findByUsernameToValidate(
      req.user.username,
    );

    if (!user)
      throw new UnauthorizedException('User attempted operation is not found');

    const isMatch = await bcrypt.compare(
      userNewPassDTO.password,
      user.password,
    );

    if (!isMatch) throw new UnauthorizedException('Invalid password');

    const result = await this.userService.changePassword(
      user.id,
      userNewPassDTO.newPassword,
    );
    res.status(result.code).json(result.content);
  }
}

/// For Admin
@Controller('admin/user')
export class AdminUsersController {
  constructor(private userService: UsersService) {}

  @Roles(UserRole.ALPHA, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post()
  async create(@Body() body, @Res() res, @Req() req) {
    const user = new UserDTO();
    user.role = body.role;
    user.username = body.username;
    user.displayName = body.displayName;
    user.email = body.email;
    user.password = body.password;
    user.phone = body.phone;

    if (body.role <= UserRole.ALPHA) throw new UnauthorizedException();

    if (req.user.role >= user.role) throw new UnauthorizedException();

    await validate(user).then((errors) => {
      if (errors.length > 0) {
        throw new BadRequestException(
          errors[0].constraints[Object.keys(errors[0].constraints)[0]],
        );
      }
    });

    const result = await this.userService.createNewUser(user);
    res.status(result.code).json(result.content);
  }

  @Roles(UserRole.ALPHA, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete('/:id')
  async deleteUserById(@Req() req, @Res() res, @Param('id') id) {
    const user = await this.userService.findOne(id);

    if (!user) throw new BadRequestException('User with given id is not found');

    if (req.user.role >= user.role) throw new UnauthorizedException();

    if (user.role == UserRole.ALPHA) throw new UnauthorizedException();

    const result = await this.userService.deleteOneUser(id);

    res.status(result.code).json(result.content);
  }

  @Roles(UserRole.ALPHA, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Put('/:id')
  async updateUserInfo(@Req() req, @Res() res, @Param('id') id, @Body() body) {
    const userInfo = new UserInfoDTO();

    userInfo.displayName = body.displayName;
    userInfo.email = body.email;
    userInfo.phone = body.phone;
    userInfo.role = body.role;
    userInfo.password = body.password;

    await validate(userInfo).then((errors) => {
      if (errors.length > 0) {
        throw new BadRequestException(
          errors[0].constraints[Object.keys(errors[0].constraints)[0]],
        );
      }
    });

    if (userInfo.role <= UserRole.ALPHA) throw new UnauthorizedException();

    if (req.user.role >= userInfo.role) throw new UnauthorizedException();

    const { content: user } = await this.userService.findOne(id);

    if (!user)
      throw new BadRequestException('User from the given id is not found');

    if (req.user.role >= user.role) throw new UnauthorizedException();

    const result = await this.userService.updateUserInfo(id, userInfo);
    res.status(result.code).json(result.content);
  }

  @Roles(UserRole.ALPHA, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  getProfile(@Req() req) {
    return req.user;
  }

  @Roles(UserRole.ALPHA, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('all')
  async getAllUsers(@Res() res) {
    const result = await this.userService.getAllUser();
    res.status(result.code).json(result.content);
  }
}
