import { UserInfoDTO } from '../../db/dto/userinfo.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserDTO } from 'src/db/dto/user.dto';
import { User, UserRole } from 'src/db/entities/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  /**
   * Return a single user queried from id
   */
  async findOne(id: number): Promise<Record<string, any>> {
    let isOk = true;
    const result = await this.usersRepository.findOne(id).catch((error) => {
      isOk = false;
      console.log(error);
    });
    if (isOk) {
      return { code: 200, content: result };
    } else {
      return { code: 404, content: { message: 'No User Found' } };
    }
  }

  /**
   * Return a single user queried from username given
   * Include `password` attribute that favor validation
   */
  async findByUsernameToValidate(
    username: string,
  ): Promise<Record<string, any>> {
    const result = await this.usersRepository.findOne({
      select: ['id', 'username', 'password'],
      where: { username: username.toLowerCase() },
    });
    return { code: 200, content: result };
  }

  /**
   * Delete a single user from a given id
   */
  async deleteOneUser(id): Promise<Record<string, any>> {
    const result = await this.usersRepository.delete({ id }).catch((error) => {
      console.log(error);
      return {
        code: 500,
        content: {
          message: 'Something went wrong. User could not be delete.',
        },
      };
    });

    return {
      code: 200,
      content: {
        message:
          result['affected'] > 0
            ? 'User was deleted successfully'
            : 'user to be deleted is not found',
      },
    };
  }

  /**
   * Update user role
   */
  async changeUserRole(userId, role): Promise<Record<string, any>> {
    const result = await this.usersRepository
      .update(userId, { role })
      .catch((error) => {
        console.log(error);
        return {
          code: 500,
          content: {
            message: 'Could not update role',
          },
        };
      });

    return {
      code: 200,
      content: {
        message:
          result['affected'] > 0
            ? 'Role was updated successfully'
            : 'Could not update role',
      },
    };
  }

  /**
   * Return all users of every roles, and every `isActive` status
   */
  async getAllUser(): Promise<Record<string, any>> {
    let isOk = true;
    const result = await this.usersRepository.find().catch((error) => {
      isOk = false;
      console.log(error);
    });
    if (isOk) {
      return { code: 201, content: result };
    } else {
      return { code: 404, content: { message: 'No User Found' } };
    }
  }

  /**
   * Return all users of `UserRole.USER` role, and of `isActive = true`
   */
  async getAllNonAdminUser(): Promise<Record<string, any>> {
    let isOk = true;
    const result = await this.usersRepository
      .find({ role: UserRole.USER, isActive: true })
      .catch((error) => {
        isOk = false;
        console.log(error);
      });
    if (isOk) {
      return { code: 201, content: result };
    } else {
      return { code: 404, content: { message: 'No User Found' } };
    }
  }

  /**
   * Create a new user.
   * Role are optional and DEFAULT to `UserRole.USER`
   */
  async createNewUser(user: UserDTO): Promise<Record<string, any>> {
    let isOk = true;
    let message = 'Invalid request';
    let code = 400;

    const newUser = new User();
    newUser.role = user.role;
    newUser.username = user.username;
    newUser.displayName = user.displayName;
    newUser.email = user.email;
    newUser.password = user.password;
    newUser.phone = user.phone;
    const result = await newUser.save().catch((error) => {
      isOk = false;
      if (error.message.includes('duplicate key value')) {
        code = 404;
        message = 'Username is already taken!';
      } else {
        console.log(error);
      }
    });
    if (isOk) {
      return { code: 201, content: result };
    } else {
      return {
        code,
        content: { message },
      };
    }
  }

  /**
   * Update User info for `displayName`, `email`, `phone`
   */
  async updateUserInfo(
    userId,
    userInfo: UserInfoDTO,
  ): Promise<Record<string, any>> {
    let isOk = true;

    const updateSet = {};

    if (userInfo.displayName) updateSet['displayName'] = userInfo.displayName;
    if (userInfo.email) updateSet['email'] = userInfo.email;
    if (userInfo.phone) updateSet['phone'] = userInfo.phone;
    if (userInfo.role) updateSet['role'] = userInfo.role;
    if (userInfo.password) {
      const salt = await bcrypt.genSalt();
      updateSet['password'] = await bcrypt.hash(userInfo.password, salt);
    }

    if (Object.keys(updateSet).length == 0)
      return {
        code: 400,
        content: { message: 'Nothing to update!' },
      };

    await this.usersRepository.update(userId, updateSet).catch((error) => {
      isOk = false;
      console.log(error);
    });

    if (isOk) {
      return { code: 200, content: { message: 'Updated user successfully' } };
    } else {
      return {
        code: 400,
        content: { message: 'Invalid request' },
      };
    }
  }

  /**
   * Change the user's password
   */
  async changePassword(userId, newPassword): Promise<Record<string, any>> {
    let isOk = true;

    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(newPassword, salt);

    await this.usersRepository
      .update(userId, { password: hashedPassword })
      .catch((error) => {
        isOk = false;
        console.log(error);
      });

    if (isOk) {
      return {
        code: 200,
        content: { message: 'Password changed successfully' },
      };
    } else {
      return {
        code: 400,
        content: { message: 'Invalid request' },
      };
    }
  }
}
