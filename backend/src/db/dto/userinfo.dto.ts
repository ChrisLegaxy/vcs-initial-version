import {
  IsEmail,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  Length,
  Matches,
} from 'class-validator';
export class UserInfoDTO {
  @IsOptional()
  @Matches('^[a-zA-Z0-9_ ]*$')
  @Length(4, 16)
  displayName: string;

  @IsOptional()
  @IsString()
  @IsEmail()
  email: string;

  @IsOptional()
  @IsNumberString()
  phone: string;

  @IsOptional()
  @IsNumber()
  role: number;

  @IsOptional()
  @IsString()
  @Length(8)
  password: string;
}
