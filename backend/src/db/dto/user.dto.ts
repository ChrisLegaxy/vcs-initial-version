import {
  IsString,
  Length,
  IsNumberString,
  IsEmail,
  Matches,
} from 'class-validator';
export class UserDTO {
  role: number;

  @IsString()
  @Length(4, 16)
  username: string;

  @IsString()
  @IsEmail()
  email: string;

  @IsString()
  @Length(8)
  password: string;

  @Matches('^[a-zA-Z0-9_ ]*$')
  @Length(4, 16)
  displayName: string;

  @IsNumberString()
  phone: string;
}
