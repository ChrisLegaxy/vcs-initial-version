import { IsString, Length } from 'class-validator';

export class UserNewPasswordDTO {
  @IsString()
  @Length(8)
  password: string;

  @IsString()
  @Length(8)
  newPassword: string;
}
