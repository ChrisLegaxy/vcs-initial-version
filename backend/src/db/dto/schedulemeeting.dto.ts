import { IsString, IsNumber, IsISO8601 } from 'class-validator';
export class ScheduledMeetingDTO {
  @IsString()
  title: string;

  @IsISO8601({ strict: true })
  start_date: string;

  @IsISO8601({ strict: true })
  end_date: string;

  @IsNumber({}, { each: true })
  attendeeIds: number[];
}
