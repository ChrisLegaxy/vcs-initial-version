import {MigrationInterface, QueryRunner} from "typeorm";

export class initializeTables1634401726584 implements MigrationInterface {
    name = 'initializeTables1634401726584'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."user_role_enum" AS ENUM('0', '1', '2', '3')`);
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "role" "public"."user_role_enum" NOT NULL DEFAULT '2', "username" character varying(16) NOT NULL, "email" character varying(500) NOT NULL, "password" character varying(500) NOT NULL, "displayName" character varying(20) NOT NULL, "isActive" boolean NOT NULL DEFAULT true, "phone" character varying(15), "createdAt" TIMESTAMP NOT NULL DEFAULT 'NOW()', CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "meeting" ("id" character varying NOT NULL, "title" character varying, "start_date" TIMESTAMP, "end_date" TIMESTAMP, "createdAt" TIMESTAMP NOT NULL DEFAULT 'NOW()', "ownerId" integer, CONSTRAINT "PK_dccaf9e4c0e39067d82ccc7bb83" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_invited_meetings_meeting" ("userId" integer NOT NULL, "meetingId" character varying NOT NULL, CONSTRAINT "PK_0872a30359770f3bee87813c1c5" PRIMARY KEY ("userId", "meetingId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_ff1d3feb478c1deeea392d1fa7" ON "user_invited_meetings_meeting" ("userId") `);
        await queryRunner.query(`CREATE INDEX "IDX_509e488816e0652753b56f640a" ON "user_invited_meetings_meeting" ("meetingId") `);
        await queryRunner.query(`ALTER TABLE "meeting" ADD CONSTRAINT "FK_c0185d913cae81475efd50349fc" FOREIGN KEY ("ownerId") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_invited_meetings_meeting" ADD CONSTRAINT "FK_ff1d3feb478c1deeea392d1fa71" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "user_invited_meetings_meeting" ADD CONSTRAINT "FK_509e488816e0652753b56f640ad" FOREIGN KEY ("meetingId") REFERENCES "meeting"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_invited_meetings_meeting" DROP CONSTRAINT "FK_509e488816e0652753b56f640ad"`);
        await queryRunner.query(`ALTER TABLE "user_invited_meetings_meeting" DROP CONSTRAINT "FK_ff1d3feb478c1deeea392d1fa71"`);
        await queryRunner.query(`ALTER TABLE "meeting" DROP CONSTRAINT "FK_c0185d913cae81475efd50349fc"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_509e488816e0652753b56f640a"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ff1d3feb478c1deeea392d1fa7"`);
        await queryRunner.query(`DROP TABLE "user_invited_meetings_meeting"`);
        await queryRunner.query(`DROP TABLE "meeting"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TYPE "public"."user_role_enum"`);
    }

}
