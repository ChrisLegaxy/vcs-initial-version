import { nanoid } from 'nanoid';
import {
  Entity,
  BeforeInsert,
  BaseEntity,
  PrimaryColumn,
  Column,
  ManyToOne,
  ManyToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Meeting extends BaseEntity {
  @PrimaryColumn('varchar')
  id: string;

  @Column({ nullable: true })
  title: string;

  @Column({ nullable: true })
  start_date: Date;

  @Column({ nullable: true })
  end_date: Date;

  @ManyToOne(() => User, (user) => user.ownedMeetings, { onDelete: 'SET NULL' })
  owner: User;

  @ManyToMany(() => User, (user) => user.invitedMeetings)
  attendees: User[];

  @CreateDateColumn({ type: 'timestamp without time zone' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp without time zone' })
  updatedAt: Date;

  @BeforeInsert()
  async setId() {
    this.id = await nanoid(12);
  }
}
