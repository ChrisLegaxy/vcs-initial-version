import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  BeforeInsert,
  BaseEntity,
  OneToMany,
  ManyToMany,
  JoinTable,
  UpdateDateColumn,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Meeting } from './meeting.entity';

export enum UserRole {
  ALPHA = 0,
  ADMIN = 1,
  USER = 2,
  ROUGE = 3,
}

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.USER,
  })
  role: number;

  @Column({ unique: true, length: 16 })
  username: string;

  @Column({ length: 500 })
  email: string;

  @Column({ select: false, length: 500 })
  password: string;

  @Column({ length: 20 })
  displayName: string;

  @Column({ default: true })
  isActive: boolean;

  @Column({ length: 15, nullable: true })
  phone: string;

  @OneToMany(() => Meeting, (meeting) => meeting.owner)
  ownedMeetings: Meeting[];

  @ManyToMany(() => Meeting, (meeting) => meeting.attendees, {
    onDelete: 'CASCADE',
  })
  @JoinTable()
  invitedMeetings: Meeting[];

  @CreateDateColumn({ type: 'timestamp without time zone' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp without time zone' })
  updatedAt: Date;

  @BeforeInsert()
  async setPasswordAndUsername(password: string) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(password || this.password, salt);
    this.username = this.username.toLowerCase();
  }
}
