import { Seeder, Factory } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { User } from '../entities/user.entity';
import * as bcrypt from 'bcrypt';

export default class CreateUsers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash('p@$$', salt);
    await connection
      .createQueryBuilder()
      .insert()
      .into(User)
      .values([
        {
          role: 0,
          username: 'alpha',
          email: 'alpha1003@gmail.com',
          displayName: 'Alpha',
          password: password,
          phone: '015958841',
        },
      ])
      .execute();
  }
}
