import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

const config: PostgresConnectionOptions = {
  database: 'vcs',
  host: 'db',
  port: 5432,
  username: 'postgres',
  password: 'i9qumViEsZTkFZYb1gTbWEEuTX6YR9/JxGSiucVDSv4=',
  type: 'postgres',
  entities: ['dist/src/**/*.entity.js'],
  synchronize: false,
  migrations: ['dist/src/db/migrations/*.js'],
  cli: {
    migrationsDir: 'src/db/migrations/',
  },
  ...{ seeds: ['dist/src/db/seeds/*.js'] },
};

export default config;
