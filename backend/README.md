
# VCS - NEST.js - API

  

Backend for VCS

  

## Installation

### 1. Configure Database Config
Adjust database configuration in `ormconfig.js` as shown in the example :

```javascript
const  config:  PostgresConnectionOptions  =  {

	database:  'vcs_development',

	host:  '127.0.0.1',

	port:  5432,

	username:  'postgres',

	password:  '1003',

	type:  'postgres',

	entities: ['dist/src/**/*.entity.js'],

	synchronize:  false,

	migrations: ['dist/src/db/migrations/*.js'],

	cli:  {

	migrationsDir:  'src/db/migrations/',

	},

	...{ seeds: ['dist/src/db/seeds/*.js'] },

};
```

Create `.env` environment file following the example `.env.example` environment provided
```javascript
JWT_SECRET=SUPERSECRET
```
### 2. Install Dependencies & Database Migration
```bash

# Install dependencies
$ npm install

# Migrate database tables
$ npm run migration:run

# Seed database to create ALPHA user
# Username = alpha
# Password = p@$$
$ npm run seed:run

```

  

## Running the app

  

```bash

# development

$ npm run start

  

# watch mode

$ npm run start:dev

  

# production mode

$ npm run start:prod

```

## Utilities Command Line

  

```bash

# To create migration file for database
# Should run after create/modified a *.enitity.js
$ npm run migration:generate -- your_migration_file_name_here

```

# Route Testing 
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/9755381-32ae9f2e-87eb-4e00-b8b8-582ea11a917e?action=collection%2Ffork&collection-url=entityId%3D9755381-32ae9f2e-87eb-4e00-b8b8-582ea11a917e%26entityType%3Dcollection%26workspaceId%3D2ffc360c-32a5-4bbe-8d59-08a304272799)
